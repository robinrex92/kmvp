package `in`.robinrex.kmvp.base

import android.support.annotation.StringRes


/**
 * The base view interface that will be extended by any activity or fragment component in the app.
 *
 * @author ContusTeam <developers></developers>@contus.in>
 * @version 1.0
 */

interface IBaseView {

    /**
     * Shows a short toast message.
     *
     * @param message The message to be shown in the toast.
     */
    fun showToast(message: String)

    /**
     * Shows a short toast message.
     *
     * @param stringId The resource id of the string to be shown in toast.
     */
    fun showToast(@StringRes stringId: Int)

    /**
     * Shows a loading screen on top of the activity. Could be used while doing long running operations such as
     * network call or loading from a local database.
     */
    fun showLoading()

    /**
     * Hides the loading screen invoked by [.showLoading] method call. Only hides if the loading dialog has
     * already been showing. It does nothing otherwise.
     */
    fun hideLoading()

}