package `in`.robinrex.kmvp.base

import android.util.Log

object KLog {
    fun l(content: Any) {
        Log.d("KMVP", content.toString())
    }
}