package `in`.robinrex.kmvp.base

/**
 * Base presenter class that contains abstract and other common methods used by all other presenter implementations.
 *
 * @param <T> The type of the view that is mapped to this presenter.
 * @author ContusTeam <developers></developers>@contus.in>
 * @version 1.0
</T> */

/**
 * Constructor for this presenter.
 */
abstract class BasePresenter<T : IBaseView> {

    /**
     * Returns the view interface for this presenter.
     */
    var view: T? = null
        private set

    /**
     * Tells whether a view instance is currently attached to this presenter or not.
     *
     * @return Returns true, if the view is attached to this presenter, false otherwise.
     */
    val isViewAttached: Boolean
        get() = view != null

    /**
     * Initializes all the UI components in the view.
     */
    abstract fun initViews()

    /**
     * A simple compatibility method to avoid all the [.isViewAttached] checks in older code.
     * Just pass the runnable as a lambda and run the code if the view is attached to the presenter.
     *
     * @param runnable The code block to be run if the view is still attached to the view.
     */
    inline fun withView(runOnView: T.() -> Unit) {
        view?.runOnView()
    }

    /**
     * This method will be called when any view is clicked and onclick listeners have been set for those views.
     *
     * @param viewId The id of the view that has been clicked.
     */
    fun onViewClicked(viewId: Int) {

    }

    /**
     * Sets the view reference for this presenter.
     *
     * @param view The view reference.
     */
    fun attachView(view: T) {
        this.view = view
    }

    /**
     * Detaches the view from the presenter. Should be called whenever the view is being destroyed by the system.
     */
    fun detachView() {
        this.view = null
    }

}
