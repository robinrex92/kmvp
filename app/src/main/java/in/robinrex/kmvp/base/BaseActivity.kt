package `in`.robinrex.kmvp.base

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View


/**
 * Base activity that should be extended by other activities of this application.
 * It has common methods that may be used by the extending classes.
 *
 * @param <V> The interface that represents this activity, which will be used by this activity's presenter.
 * @param <P> The presenter that will manage this activity through the view interface.
 * @author ContusTeam <developers></developers>@contus.in>
 * @version 1.0
</P></V> */

abstract class BaseActivity<V : IBaseView, P : BasePresenter<V>> : AppCompatActivity(), IBaseView, View.OnClickListener {

    lateinit var mPresenter: P

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModel: BaseViewModel<V, P> = ViewModelProviders.of(this).get(BaseViewModel::class.java) as BaseViewModel<V, P>

        if (viewModel.presenter == null) {
            viewModel.presenter = initPresenter()
        }

        mPresenter = viewModel.presenter as P

        mPresenter.attachView(getViewInterface())
    }

    protected abstract fun getViewInterface(): V

    protected abstract fun initPresenter(): P

    override fun showToast(message: String) {
        KLog.l("Showing toast")
    }

    /**
     * This is a simple helper method to set click listeners for any number of views without having to write the
     * mundane [View.setOnClickListener] code to every view you ever want to set click
     * listeners for. Instead just call this method and pass in all the views that you want click listeners attached.
     * All the views that you pass as variable arguments to this method will be having the same on click listener
     * attached to them. And just handle the click event in the
     * [android.view.View.OnClickListener.onClick] method that you must be overriding in your subclass.
     * This doesn't do much in saving developing time, but it does make the code cleaner and shorter.
     *
     * @param toViews The views as variable arguments that you want click listeners attached.
     */
    protected fun addViewClickListener(vararg toViews: View) {
        for (v in toViews) {
            v.setOnClickListener(this)
        }
    }

    override fun showToast(stringId: Int) {
        val toastMessage = "Get string from resources here"
        if (!TextUtils.isEmpty(toastMessage)) {
            //Show toast here
            KLog.l("Showing toast")
        }

    }

    override fun showLoading() {
        KLog.l("Loading dialog shown")
        //Show loading if now already showing
    }

    override fun hideLoading() {
        //Hide loading if showing
    }

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
        hideLoading()
    }
}