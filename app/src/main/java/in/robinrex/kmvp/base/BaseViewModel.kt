package `in`.robinrex.kmvp.base

import android.arch.lifecycle.ViewModel


/**
 * A simple view model to survive configuration changes.
 *
 * @param <V> The view interface that represents the activity or fragment associated with this view model.
 * @param <P> The presenter that is associated with the activity or fragment of this view model.
 * @author ContusTeam <developers></developers>@contus.in>
 * @version 1.0
</P></V> */
class BaseViewModel<V : IBaseView, P :  BasePresenter<V>> : ViewModel() {

    /**
     * Returns the presenter instance attached to this view model.
     *
     * @return The presenter instance mapped to a specific instance of an activity or a fragment.
     */
    /**
     * Sets the instance of presenter to this view model. This presenter instance will be used for recreated
     * activities and fragments.
     *
     * @param mPresenter The presenter instance to be attached to this view model.
     */
    var presenter: P? = null

    override fun onCleared() {
        super.onCleared()

        if (presenter != null) {
            presenter!!.detachView()
            presenter = null
        }
    }
}