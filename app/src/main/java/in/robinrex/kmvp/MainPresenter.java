package in.robinrex.kmvp;

import in.robinrex.kmvp.base.BasePresenter;

public class MainPresenter extends BasePresenter<IMainView> {

    @Override
    public void initViews() {
        getView().hideLoading();
    }
}
