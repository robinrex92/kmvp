package in.robinrex.kmvp;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import org.jetbrains.annotations.NotNull;

import in.robinrex.kmvp.base.BaseActivity;

public class MainActivity extends BaseActivity<IMainView, TestPresenter> implements IMainView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getMPresenter().initViews();

    }

    @NotNull
    @Override
    protected TestPresenter initPresenter() {
        return new TestPresenter();
    }

    @NotNull
    @Override
    protected IMainView getViewInterface() {
        return this;
    }

    @Override
    public void onClick(View v) {
    }

}
