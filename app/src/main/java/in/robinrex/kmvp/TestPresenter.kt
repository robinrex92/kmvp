package `in`.robinrex.kmvp

import `in`.robinrex.kmvp.base.BasePresenter

class TestPresenter : BasePresenter<IMainView>() {
    override fun initViews() {
        withView { showLoading() }
    }
}